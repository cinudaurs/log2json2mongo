# README #

A simple log parser that converts log lines into json before importing into mongodb for data analysis on method calls and response_times. Using mongodb queries, draw insights on interesting data points like: the frequency of the method call, the maximum response times, times when the frequency of specific calls was higher, calculate mean, median and standard deviation for various calls.
