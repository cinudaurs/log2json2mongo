#!/usr/bin/env python

import json, re, sys, pymongo, math


connection = pymongo.Connection("mongodb://localhost", safe=True)
 # get a handle to the oupeac database
db = connection.oupeac                 # attach to db
collection = db.querytimings        # specify the collection

if sys.argv > 3:
	infile = sys.argv[1]
	query_name = sys.argv[2]
	start_date = sys.argv[3]
	end_date = sys.argv[4]
else:
	print " usage : python log2json2mongo.py infile \"query_name\" \"start_date\" \"end_date\" "
	print " start_date and end_date format should be : yyyy-mm-ddThh:mm:ss"

outformatfile = infile + '.format'
outfile = infile + '.minmax'

re_kv = re.compile(r'(\w+)=(\S*)')


out = open(outfile,'w')

outformat = open(outformatfile, 'w')

def formatlog():

   	print "1. formatting the log to desired format"
   	for line in open(infile,'r').readlines():
	       	matchMs = re.search(r"(\d+)(ms)(,)", line)
	 	matchTime = re.match('^(\d+)-(\d+)-(\d+)\s+(\d+):(\d+):(\d+)(?:,(\d+))?',line)
       		if (matchMs and matchTime):
       			line = line.replace(matchMs.group(), matchMs.group(1)+" ")
       			outformat.write(line.replace(matchTime.group(),"date="+matchTime.group(1)+"-"+matchTime.group(2)+"-"+matchTime.group(3)+"T"+matchTime.group(4)+":"+matchTime.group(5)+":"+matchTime.group(6)))

def insert():
                print "2. Inserting json data into MongoDB"

	        for line in open(outformatfile,'r').readlines():

          	     	js = dict(re_kv.findall(line))
                        
                        #printplus(js)
                        
                        if 'name' in js.keys():  
     				mongoDoc = {"name":js['name'].strip(','), "time":js['time'], "date":js['date']}
                        else:
				break				
        
	      	        try:
		    		collection.insert(mongoDoc)	
        		except:
            			print "Unexpected error:", sys.exc_info()[0]
                        
                        outformat.close()  


def find():

	        print "3. querying for data..."
   
    #query = {"name":"LOOKUP_CURRENT_TABLEID,","date": {"$lte": "2008-01-21T20:10:36", "$gte":"2008-01-21T20:08:28"} }
	        query = {"name":query_name ,"date": {"$gte": start_date, "$lte":end_date} }
		selector =  {"time":1,"_id":0}

    		try: 
	
		       c = collection.find(query,selector)
       		       c = c.sort([("time",pymongo.ASCENDING)])
	        except:
		       print "Unexpected error:", sys.exc_info()[0]

    		for doc in c:
	               out.writelines(doc['time']+"\n")	
                
		out.close() 


def displayStats():

        f = open(outfile, 'r')
        data_list = f.readlines()
        data_list = map(float, data_list)
        n = len(data_list)

        print "Count :", n

        print "Min :", min(data_list)

        print "10 percentile :", data_list[int(math.ceil(n*0.1))]

        print "25 percentile :" , data_list[int(math.ceil(n*0.25))]

        # Test whether the n is odd
        if n & 1:
                # If is is, get the index simply by dividing it in half
                index = n / 2
                print data_list[index]
        else:
                # If the n is even, average the two values at the center
                low_index = n / 2 - 1
                high_index = n / 2
                average = (data_list[low_index] + data_list[high_index]) / 2

                print "Median :", average

        print "75 percentile :", data_list[int(math.ceil(n*0.75))]

        print "90 percentile :", data_list[int(math.ceil(n*0.9))]

        print "99 percentile :", data_list[int(math.ceil(n*0.99))]

        print "99.5 percentile :", data_list[int(math.ceil(n*0.995))]

        print "99.9 percentile :", data_list[int(math.floor(n*0.999))]

        print "Max :", max(data_list)

        mean = sum(data_list) / n
        print "Mean :", mean

        deviations = [i - mean for i in data_list]
        deviations_squared = [math.pow(i, 2) for i in deviations]
        mean_deviation = sum(deviations_squared) / len(deviations_squared)
        standard_deviation = math.sqrt(mean_deviation)

        print "std deviation :", standard_deviation

        f.close()


def printplus(obj):
    """
    Pretty-prints the object passed in.

    """
    # Dict
    if isinstance(obj, dict):
        for k, v in sorted(obj.items()):
            print u'{0}: {1}'.format(k, v)

    # List or tuple            
    elif isinstance(obj, list) or isinstance(obj, tuple):
        for x in obj:
            print x

    # Other
    else:
        print obj




formatlog()
insert()
find()
displayStats()

